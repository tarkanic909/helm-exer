{{- define "name.getName" -}}
{{- .Chart.Name }}
{{- end }}

{{- define "webApp.getLabel" -}}
{{- "app: " -}}
{{ .Release.Name }}-{{ .Chart.Name }}
{{- end }}

{{- define "webApp.getNamespace" -}}
{{- "namespace:jtarkanic" }}
{{- end}}

{{- define "service.getName" -}}
{{- "webapp--svc" }}
{{- end}}
